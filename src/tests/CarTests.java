//Aharon Moryoussef 
//Dan Pomerantz
//1732787

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	void testCar() {
		Car astonMartin = new Car(190);
		System.out.println(astonMartin.getSpeed());
		
		try {
			Car bmw = new Car(-190); //Should throw an exception.
			fail("Speed must not be negative!");
			
		} catch(IllegalArgumentException e) {
			
		}
		catch(Exception e) {
			fail("Speed must not be negative!");	
		}
	}

	@Test
	void testGetSpeed() {
		Car astonMartin = new Car(190);
		assertEquals(190,astonMartin.getSpeed());
	}

	@Test
	void testGetLocation() {
		Car astonMartin = new Car(0);
		assertEquals(0,astonMartin.getLocation());
	}

	@Test
	void testMoveRight() {
		Car astonMartin = new Car(2);
		astonMartin.moveRight();
		assertEquals(2,astonMartin.getLocation());
	}

	@Test
	void testMoveLeft() {
		Car astonMartin = new Car(8);
		astonMartin.moveLeft();
		assertEquals(-8,astonMartin.getLocation());
		
	}

	@Test
	void testAccelerate() {
		Car astonMartin = new Car(8);
		astonMartin.accelerate();
		assertEquals(9,astonMartin.getSpeed());
		
		
		Car tesla = new Car(0);
		tesla.accelerate();
		assertEquals(1,tesla.getSpeed());
		
		Car bmw = new Car(4);
		bmw.accelerate();
		assertEquals(5,bmw.getSpeed());
	}

	@Test
	void testDecelerate() {
		Car astonMartin = new Car(5);
		astonMartin.decelerate();
		assertEquals(4,astonMartin.getSpeed());
		
		Car tesla = new Car(0);
		tesla.decelerate();
		assertEquals(0,tesla.getSpeed());
		
		Car bmw = new Car(4);
		bmw.decelerate();
		assertEquals(3,bmw.getSpeed());
	}

}
